﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager:MonoBehaviour
{
    private GameObject PauseMenuObject;
    private GameObject InventoryObject;
    private GameObject MiscElementsObject;
    private GameObject MainScreen;

    public Canvas PauseMenuCanvas;
    public Canvas InventoryCanvas;
    public Canvas MiscElementsCanvas;

    void Start()
    {
        PauseMenuObject = GameObject.Find("MenuCanvas");
        InventoryObject = GameObject.Find("InventoryCanvas");
        MiscElementsObject = GameObject.Find("MenuButton");

        if (PauseMenuObject != null)
            PauseMenuCanvas = PauseMenuObject.GetComponent<Canvas>();
        if (InventoryObject != null)
            InventoryCanvas = InventoryCanvas.GetComponent<Canvas>();
        if (MiscElementsObject != null)
            MiscElementsCanvas = MiscElementsCanvas.GetComponent<Canvas>();

    }

    public void TogglePauseMenu()
    {
        if (InventoryCanvas.enabled)
        {
            InventoryCanvas.enabled = false;
            PauseMenuCanvas.enabled = true;
            MiscElementsCanvas.enabled = false;

            Time.timeScale = 1.0f;
        }
        else
        {
            InventoryCanvas.enabled = true;
            PauseMenuCanvas.enabled = false;
            MiscElementsCanvas.enabled = true;

            Time.timeScale = 0f;
        }
    }


    void Update()
    {
        ScanForKeyStroke();
    }

    private void ScanForKeyStroke()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            TogglePauseMenu();
    }

    public void Quit()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    public void QuitToMainMenu(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }
}