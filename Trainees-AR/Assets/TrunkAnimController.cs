﻿using UnityEngine;

namespace Assets
{
    public class TrunkAnimController : MonoBehaviour {

        private Animator _anim;

        private void Start ()
        {
            _anim = GetComponent<Animator>();
        }
	
        private void Update () {
            //if (Input.GetMouseButtonDown(0))
            //{
            //_anim.Play("TrunkAnimation");
            //}
        }

        public void OpenChest()
        {
            _anim.Play("TrunkAnimation");
        }
    }
}
