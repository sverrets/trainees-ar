﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioController
{
    public Slider Slider { get; set; }
    public AudioSource AudioSource { get; set; }
    public string AudiSourceName { get; set; }
}
