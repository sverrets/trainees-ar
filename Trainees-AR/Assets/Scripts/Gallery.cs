﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gallery : MonoBehaviour {

    public GameObject[] artworks;
    public GameObject inventory;
    public HintsManager hintsManager;

    private bool gameStarted = false;
    private bool hintDisplayed = true;
    private int artworksCompleted = 0;

	// Use this for initialization
	void Start () {
        if (!inventory)
            Debug.LogError("Gallery needs to have an inventory attached, check object in inspector");
        if (!hintsManager)
            Debug.LogError("Gallery needs to have a hintsManager attached, check object in inspector");
    }
	
	// Update is called once per frame
	void Update () {
        inventory.SetActive(!hintDisplayed);
	}

    public bool IsGameStarted()
    {
        return gameStarted;
    }

    public void StartGame()
    {
        gameStarted = true;
        inventory.SetActive(true);
    }

    public void ToggleHintDisplayed()
    {
        Debug.Log("toggle");
        hintDisplayed = !hintDisplayed;
            inventory.SetActive(!hintDisplayed);
    }

    public void TriggerHintFromItemPickedUp(GameObject item)
    {
        string itemName = item.name.Replace("(Clone)", "");
        foreach (GameObject artwork in artworks)
        {    
            Artwork artworkScript = artwork.GetComponent<Artwork>();

            if (artworkScript.correctItem && artworkScript.correctItem.name.Equals(itemName))
                hintsManager.SetHintText(artwork.GetComponent<Artwork>().hint);
        }
    }
}
