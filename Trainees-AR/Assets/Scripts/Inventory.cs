﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public Gallery gallery;
    public GameObject[] itemSlots;
    public GameObject selectedItemEffect;
    public GameObject selectedItemText;

    private GameObject selectedItem;
    private Ray ray;
    private RaycastHit hit;

    private void Start()
    {
        ResetItemText();
    }

    private void ResetItemText()
    {
        selectedItemText.GetComponent<Text>().text = "";
    }

    public void SetSelected(GameObject selectedItemSlot, GameObject itemClicked)
    {
        DestroySelectedItemEffect();
        var newSelectedEffect = Instantiate(selectedItemEffect, selectedItemSlot.transform);
        newSelectedEffect.transform.SetSiblingIndex(0);
    }

    public void DestroySelectedItemEffect()
    {
        foreach (GameObject itemSlot in itemSlots)
        {
            foreach (Transform child in itemSlot.transform)
            {
                if (child.name.StartsWith("SelectedItemGlow"))
                    Destroy(child.gameObject);
            }
        }
        ResetItemText();
    }

    public Transform FindEmptySlot()
    {
        foreach (Transform slot in gameObject.transform)
        {
            if (slot.gameObject.tag == "slotEmpty")
                return slot;    
        }
        return null;
    }

    public void SpawnItemAndPickUp(GameObject spawnItem)
    {
        var emptySlot = FindEmptySlot();
        var newItem = Instantiate(spawnItem, FindEmptySlot());

        emptySlot.gameObject.tag = "slotFull";
        newItem.transform.position = Input.mousePosition;
        StartCoroutine(PickUpItem(newItem));
    }

    public void AddItem(GameObject item)
    {
        var emptySlot = FindEmptySlot();
        emptySlot.gameObject.tag = "slotFull";
        StartCoroutine(SpawnItem(item, emptySlot));
    }

    public IEnumerator SpawnItem(GameObject item, Transform emptySlot)
    {
        var newItem = Instantiate(item, emptySlot);
        var centerPos = new Vector3(Screen.width / 2, Screen.height / 2);
        var parentPos = newItem.transform.parent.position;
        newItem.transform.position = centerPos;

        var t = 0f;
        var timeToScale = .7f;
        
        while (t < timeToScale)
        {
            t += Time.deltaTime / timeToScale;
            newItem.transform.localScale = Vector3.Lerp(new Vector3(0.1f, 0.1f, 0.1f), new Vector3(10f, 10f, 10f), t);
            yield return null;
        }
    }

    public IEnumerator PickUpItem(GameObject item)
    {
        var t = 0f;
        var timer = .7f;
        while (t < timer)
        {
            t += Time.deltaTime / timer;
            item.transform.position = Vector3.Lerp(item.transform.position, item.transform.parent.position, t);
            item.transform.localScale = Vector3.Lerp(item.transform.localScale, new Vector3(1f,1f,1f), t);
            yield return null;
        }
        if (!item.GetComponent<Draggable>().inInventory)
        {
            gallery.TriggerHintFromItemPickedUp(item);
            item.GetComponent<Draggable>().inInventory = true;
        }
    }
}