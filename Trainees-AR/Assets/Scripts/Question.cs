﻿using UnityEngine;
using UnityEngine.UI;

public class Question : MonoBehaviour {

    private GameObject artwork;

    [Space(10)]
    public string[] answers = new string[4];

    [Tooltip("element position of correct answer in the above array")]
    [Range(0,3)]
    public int correctAnswerElement = 0;

    void Start(){
        artwork = transform.parent.parent.gameObject;

        if (answers[0] != "")
        {
            int i = 0;
            foreach (Transform answerUI in transform.GetChild(0))
            {
                answerUI.GetChild(0).GetComponent<Text>().text = answers[i];
                i++;
            }
        }
    }

    public void handleAnswerClicked(Transform answer){
        if (answer.parent.GetChild(correctAnswerElement) == answer)
        {
            answer.GetChild(0).GetComponent<Text>().text = "RIKTIG!";
            artwork.GetComponent<Artwork>().CorrectAnswer();
        }else
            answer.GetChild(0).GetComponent<Text>().text = "FEIL!";
    }
}
