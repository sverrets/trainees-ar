﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Draggable : MonoBehaviour {
    private bool isDragging = false;
    private Inventory inventory;
    private Ray ray;
    private RaycastHit hit;

    public bool inInventory = false;
    public GameObject itemTarget;

    void Start () {
        inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
    }
	
	void Update () {
	    
        if (isDragging)
        {
            gameObject.GetComponent<Transform>().position = Input.mousePosition;
        }
    }

    public void startDrag()
    {
        isDragging = true;
    }

    public void endDrag()
    {
        isDragging = false;

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            if (hit.transform.GetComponent<ItemTarget>())
            {
                ItemTarget itemTarget = hit.transform.GetComponent<ItemTarget>();
                itemTarget.CheckIfCorrectItem(this.gameObject);
            }
        }
        this.transform.localPosition = Vector3.zero;
    }

    public void ClickOnItem()
    {
        if (inInventory)
            inventory.SetSelected(this.transform.parent.gameObject, this.gameObject);
        else
        {
            StartCoroutine(inventory.PickUpItem(this.gameObject));
        }
    }
}
