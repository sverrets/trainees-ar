﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Item : MonoBehaviour {
    public GameObject icon;

    private Inventory inventory;

    // Use this for initialization
    void Start () {
        inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
    }
	
    //OnMouseOver requires a collider on the GameObject which script is attached to.
    //void OnMouseOver()
    //{
    //    if (Input.GetMouseButtonDown(0))
    //        PickUpItem();
    //}

    public void PickUp()
    {
        inventory.SpawnItemAndPickUp(icon);
        icon.GetComponent<Draggable>().inInventory = true;
        Destroy(gameObject);
    }
}
