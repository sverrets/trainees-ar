﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectFurniture : MonoBehaviour
{
    //This Script sets active ColorPicker- and Spotlight-components for the selected GameObject.


    GameObject ColorPicker;                                                         //replace colorpicker
    GameObject Spotlight;                                                           //replace spotlight
    private Transform cube;                                                         //replace cube
    public float scaleSpeed, rotSpeed;

    private GameObject[] _colorPickerObjects;                                       //replace colorpicker
    private GameObject[] _spotlightObjects;                                         //replace spotlight
    private ColorPicker _colorPickerScript;                                         //replace colorpicker

    void Start()
    {
        ColorPicker = gameObject.transform.Find("ColorPicker").gameObject;          //replace colorPicker
        Spotlight = gameObject.transform.Find("Spotlight").gameObject;              //replace spotlight
        _colorPickerScript = ColorPicker.GetComponent<ColorPicker>();               //replace colorpicker
        cube = gameObject.transform.Find("Cube");                                   //replace cube
    }

    void Update()
    {
        float dxz = Input.GetAxis("Vertical") * scaleSpeed * Time.deltaTime;
        cube.localScale += new Vector3(dxz, 0.0f, dxz);
        cube.Rotate(Vector3.up* rotSpeed * Time.deltaTime);
    }

    private void OnMouseDown()
    {
        var activeObjects = new[]
        {
            _colorPickerObjects = GameObject.FindGameObjectsWithTag("Color picker"),    //replace colorpicker
            _spotlightObjects = GameObject.FindGameObjectsWithTag("Spotlight")          //replace spotlight
        };

        foreach (var customTag in activeObjects)
        {
            foreach (var taggedObject in customTag)
            {
                taggedObject.SetActive(false);
            }
        }

        ColorPicker.SetActive(true);                                                //replace colorpicker
        Spotlight.SetActive(true);                                                  //replace spotlight
    }
}