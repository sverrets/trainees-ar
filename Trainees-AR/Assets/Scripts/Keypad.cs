﻿using UnityEngine;
using System.Collections;

public class Keypad : MonoBehaviour
{
    public GameObject[] buttons;
    public Color displayError;
    public Color displayCorrect;
    [Range(0000, 9999)] public int correctCode = 1234;
    public GameObject display;
    public GameObject displayTextObj;

    private Color defaultDisplayColor;
    private TextMesh displayText;
    private Animator animator;
    private GameObject itemInside;
    private bool correctCodeEntered = false;
    private bool wrongCodeEntered = false;

    //Initial values for scaling/moving
    private Vector3 position;
    private Quaternion rotation;
    private Transform parent;
    private Vector3 scale;
    private Vector3 itemSize;

    private bool inFocus = false;
    private bool itemCollected = false;


    Ray ray;
    RaycastHit hit;

    public Inventory inventory;
    public GameObject reward;

    private void Start()
    {
        displayText = displayTextObj.GetComponent<TextMesh>();
        displayText.text = "";
        defaultDisplayColor = display.GetComponent<Renderer>().material.color;
        animator = gameObject.transform.GetComponent<Animator>();

        itemInside = gameObject.transform.Find("Item").gameObject;
        position = gameObject.transform.position;
        rotation = gameObject.transform.rotation;
        parent = gameObject.transform.parent;
        scale = gameObject.transform.localScale;
        itemSize = itemInside.GetComponent<BoxCollider>().size;
    }

    private void Update()
    {
        if (inFocus && itemInside == null && !itemCollected)
        {
            itemCollected = true;
            StartCoroutine(ResetPosition());
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (!inFocus)
            {
                SetInFocus();
                return;
            }
            
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                foreach (GameObject button in buttons)
                {
                    if (hit.collider == button.GetComponent<Collider>())
                    {
                        HandleButtonClick(button);
                    }
                }
                if(hit.collider == itemInside.GetComponent<Collider>() && correctCodeEntered)
                {
                    itemInside.GetComponent<Item>().PickUp();
                }
            }
        }
    }

    private void HandleButtonClick(GameObject button)
    {
        int number;
        if (int.TryParse(button.name, out number))
        {
            HandleNumberEntered(number);
        }
        else if (button.name.Equals("OK"))
        {
            SubmitCode();
        }
        else
        {
            ClearInput();
        }
    }

    private void HandleNumberEntered(int number)
    {
        if (wrongCodeEntered)
        {
            ClearInput();
            displayText.text += number;
        }else if (displayText.text.Length < 4)
        {
            displayText.text += number;
        }
    }

    private void ClearInput()
    {
        wrongCodeEntered = false;
        displayText.text = "";
        display.GetComponent<Renderer>().material.color = defaultDisplayColor;
    }

    private void SubmitCode()
    {
        if (displayText.text.Equals(correctCode.ToString()))
        {
            display.GetComponent<Renderer>().material.color = displayCorrect;
            animator.SetTrigger("keypad_open");
            correctCodeEntered = true;
            return;
        }
        wrongCodeEntered = true;
        display.GetComponent<Renderer>().material.color = displayError;
    }

    private void SetInFocus()
    {
        GameObject parentKeypadObj = gameObject.transform.parent.gameObject;
        gameObject.transform.parent = Camera.main.transform;
        gameObject.transform.localPosition = new Vector3(0, 0, 20);
        gameObject.transform.localRotation = new Quaternion(0, 0, 0, 0);
        gameObject.transform.localScale = new Vector3(.65f, .65f, .65f);
        inFocus = true;
        UpdateColliders();
    }

    private IEnumerator ResetPosition()
    {
        yield return new WaitForSeconds(1);
        gameObject.transform.parent = parent;
        gameObject.transform.position = position;
        gameObject.transform.rotation = rotation;
        gameObject.transform.localScale = scale;
    }

    private void UpdateColliders(){
        foreach (GameObject button in buttons){
            button.GetComponent<BoxCollider>().size = button.GetComponent<BoxCollider>().size;
            itemInside.GetComponent<BoxCollider>().size = itemInside.GetComponent<BoxCollider>().size;
            itemInside.GetComponent<BoxCollider>().center = itemInside.GetComponent<BoxCollider>().center;
        }
    }
}