﻿using Assets;
using UnityEngine;

public class ArtworkManager2 : MonoBehaviour {
    public GameObject questionCanvas2;
    public GameObject hintCanvas2;
    public GameObject animatedObject2;
    public GameObject chest;
    private TrunkAnimController trunkAnimController;

    private Animator _anim;

    private void Start(){
        questionCanvas2.SetActive(false);
        hintCanvas2.SetActive(true);
        animatedObject2.SetActive(false);
        _anim = GetComponent<Animator>();
    }

    public void CorrectObjectTrigger(){
        Debug.Log(this  + "--------------");
        questionCanvas2.SetActive(true);
        Destroy(hintCanvas2);
        animatedObject2.SetActive(true);
    }

    public void Reset()
    {
        questionCanvas2.SetActive(false);
        hintCanvas2.SetActive(true);
        animatedObject2.SetActive(false);
        _anim = GetComponent<Animator>();
    }

    public void OpenChest()
    {
        chest.GetComponent<Animator>().SetTrigger("openchest");
    }
}
