﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPrefsManager : MonoBehaviour
{
    public Slider MusicVolumeSlider;
    public Slider SFXVolumeSlider;

    public AudioSource MusicVolumeSource;
    public AudioSource SFXVolumeSource;

    public List<AudioController> AudioControllers;

    void Start ()
    {
        AudioControllers = new List<AudioController>()
        {
            new AudioController()
            {
                Slider = MusicVolumeSlider,
                AudioSource = 
                    MusicVolumeSource,
                AudiSourceName = "MusicVolume"
            },
            new AudioController()
            {
                Slider = SFXVolumeSlider,
                AudioSource = SFXVolumeSource,
                AudiSourceName = "SFXVolume"
            }
        };

        foreach (var audioController in AudioControllers)
        {
            audioController.Slider.value = PlayerPrefs.GetFloat(audioController.AudiSourceName, audioController.Slider.value);
            audioController.AudioSource.volume = PlayerPrefs.GetFloat(audioController.AudiSourceName, audioController.AudioSource.volume);
            audioController.Slider.onValueChanged.AddListener(delegate { SetVolume(audioController); });
        }
    }

    private void SetVolume(AudioController audioController)
    {
        audioController.AudioSource.volume = audioController.Slider.value;
        PlayerPrefs.SetFloat(audioController.AudiSourceName, audioController.Slider.value);
        PlayerPrefs.SetFloat(audioController.AudiSourceName, audioController.AudioSource.volume);
    }

    // Update is called once per frame
    void Update () {}
}
