﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Artwork : MonoBehaviour {

    private bool solved = false;
    private int questionsCompleted = 0;
    private int questionsToCompleteArtwork;
    private int objectsToActivateQuestions;
    private Inventory inventory;
    

    [TextArea(3, 10)]
    public string hint = "default hint text";
    [Tooltip("Reward for answering all questions correctly")]
    public GameObject reward;
    [Tooltip("Item that triggers questions")]
    public GameObject correctItem;
    public GameObject questionCanvas;
    [Tooltip("Object to be triggered upon using correct item")]
    public GameObject eventObject;

	void Start () {
        inventory = GameObject.Find("Inventory").GetComponent<Inventory>();

        if (questionCanvas)
        {
            questionCanvas.SetActive(false);
            questionsToCompleteArtwork = questionCanvas.transform.childCount;
        }
        if (eventObject)
            eventObject.SetActive(false);
    }

    public bool IsSolved()
    {
        return solved;
    }

    public void CorrectAnswer()
    {
        questionsCompleted++;
        if(questionsCompleted == questionsToCompleteArtwork)
        {
            solved = true;
            if(reward && inventory)
                inventory.AddItem(reward);
        }
    }

    public void CorrectObjectUsed()
    {
        if (questionCanvas)
            questionCanvas.SetActive(true);
        if (eventObject)
        {
            eventObject.SetActive(true);
        }
    }
}
