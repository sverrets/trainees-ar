﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuActions : MonoBehaviour
{
    public GameObject ButtonPanel;
    public GameObject QuitButton;
    public GameObject LoadingScreen;
    public Slider LoadingSlider;
    public TextMeshProUGUI ProgressText;

	public void StartGame (int sceneIndex)
	{
	    ButtonPanel.SetActive(false);
	    QuitButton.SetActive(false);
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        LoadingScreen.SetActive(true);
        while (!operation.isDone)
        {
            var progress = Mathf.Clamp01(operation.progress / .9f);
            LoadingSlider.value = progress;
            ProgressText.text = (Mathf.Round(progress * 100f) / 100f) * 100f + "%";
            yield return null;
        }
    }
	
	// Update is called once per frame
	void Update ()
	{
	    ScanForKeyStroke();

	}
    private void ScanForKeyStroke()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Quit();
    }
    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }
}
