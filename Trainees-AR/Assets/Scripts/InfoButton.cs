﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoButton : MonoBehaviour
{
    public GameObject InfoPanel;

    public Text InfoText;

    public GameObject MiscElements;

    public GameObject CloseButton;

	// Use this for initialization
	void Start ()
	{
        InfoPanel   = GameObject.Find("Panel").GetComponent<GameObject>();
	    InfoText = InfoPanel.GetComponentInChildren<Text>();
        MiscElements= GameObject.Find("MiscElementsCanvas").GetComponent<GameObject>();
        CloseButton = GameObject.Find("CloseButton").GetComponent<GameObject>();
	}

    public void ShowUIElements()
    {
        InfoPanel.SetActive(true);
        InfoText.enabled = true;
        MiscElements.SetActive(false);
        CloseButton.SetActive(true);
    }

	// Update is called once per frame
	void Update () {
		
	}
}
