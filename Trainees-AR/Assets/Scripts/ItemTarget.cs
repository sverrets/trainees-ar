﻿using UnityEngine;


public class ItemTarget : MonoBehaviour {
    private Artwork artwork;
    public GameObject animatedObject;

    void Start () {
        artwork = GetComponentInParent<Artwork>();
	}

    public void CheckIfCorrectItem(GameObject item)
    {
        string itemName = item.name.Replace("(Clone)", ""); ;
        if (artwork.correctItem.name == itemName)
        {
            artwork.CorrectObjectUsed();
            DestroyItem(item);
            item.transform.parent.gameObject.tag = "slotEmpty";

            if(animatedObject)
                animatedObject.GetComponent<Animator>().SetTrigger("openchest");
        }
        else
            item.transform.localPosition = Vector3.zero;
    }

    private void DestroyItem(GameObject item)
    {
        foreach (Transform child in item.transform.parent)
            Destroy(child.gameObject);
    }
}
