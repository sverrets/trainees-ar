﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HintsManager : MonoBehaviour {
    public Gallery gallery;
    public TextMeshProUGUI hintText;
    public Button closeButton;
    public GameObject hintPanel;

    private int currentHint = -1;
    private GameObject[] artworks;
    

    void Start () {
        artworks = gallery.artworks;
        closeButton.onClick.AddListener(CloseHint);
    }

    void NextHint()
    {
        currentHint++;
        hintText.text = artworks[currentHint].GetComponent<Artwork>().hint;
    }

    void DisplayHint()
    {
        hintPanel.SetActive(true);
        gallery.ToggleHintDisplayed();
    }

    public void SetHintText(string hint)
    {
        hintText.text = hint;
        DisplayHint();
    }

    void CloseHint()
    {
        if (!gallery.IsGameStarted())
        {
            gallery.StartGame();
            NextHint();
        }
        else
        {
            hintPanel.SetActive(false);
            gallery.ToggleHintDisplayed();
        }
    }
}
