﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BackButton : MonoBehaviour
{
    public void HideHintText()
    {
        foreach (Transform child in transform)
        {
            child.GetComponent<TextMeshProUGUI>().enabled = false;
        }
    }
}
